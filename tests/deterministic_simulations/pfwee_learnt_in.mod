/* Tests perfect_foresight_with_expectation_errors_{setup,solver}
   using the shocks(learnt_in=…), mshocks(learnt_in=…) and endval(learnt_in=…) syntax */

@#define bare_first_info_period = false
@#define dates = false

@#include "pfwee_learnt_in.inc"
